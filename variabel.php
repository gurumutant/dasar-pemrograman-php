<html>
<head>
	<title>PHP Variables</title>
</head>
<body>
	<h1>RPL PHP Programming 2019</h1>
	<h2>Variabel di PHP</h2>
	<?php
		// inisialisasi variabel
		$nama = "Ferguso";
		$Nama = "Santoso";
		// echo menggunakan petik dua
		echo "Hello World, $Nama<br>";
		echo "bandingkan dengan :<br>";
		// echo menggunakan petik satu
		echo 'Hello World, $Nama<br>';
	?>
</body>
</html>