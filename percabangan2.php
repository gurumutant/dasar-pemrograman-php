<html>
<head>
	<title>PHP Control Structures : Switch</title>
</head>
<body>
	<h1>RPL PHP Programming 2019</h1>
	<h2>Percabangan menggunakan switch .. case</h2>
	<?php 
		// isset: mengecek suatu variabel diset apa tidak
		// $_GET: superglobal berupa array URL parameters
		// jika $_GET["d"] ada, ambil nilai dari situ.
		// jika tidak, isi dengan 0
		$h = (isset($_GET["d"])) ? $_GET["d"] : 0;
		switch ($h) {
			case 0: echo "Hari Minggu"; break;
			case 1: echo "Hari Senin"; break;
			case 2: echo "Hari Selasa"; break;
			case 3: echo "Hari Rabu"; break;
			case 4: echo "Hari Kamis"; break;
			case 5: echo 'Hari Jum\'at'; break;
			case 6: echo "Hari Sabtu"; break;
			default: echo "hari tidak terdefinisi";
		}

		echo "<br><br>Tanggal ".date("d-m-Y");
		echo "<br><br>Index hari : ".date("w");
		$namahari = ["Minggu","Senin","Selasa","Rabu","Kamis","Jum'at","Sabtu"];
		echo "<br><br>Hari ini adalah hari : ".$namahari[date("w")];
	?>
</body>
</html>