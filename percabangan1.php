<html>
<head>
	<title>PHP Control Structures : IF</title>
</head>
<body>
	<h1>RPL PHP Programming 2019</h1>
	<h2>Percabangan menggunakan if</h2>
	<?php
		if (date("D") == "Mon")
			$hari = "Senin";
		elseif (date("D") == "Tue")
			$hari = "Selasa";
		else
			$hari = "hari yang lain";
		echo "Hari ini adalah hari ".$hari;	

		// syntax alternatif
		if (date("Y") > 2012):
			echo "<br>ternyata belum jadi kiamat";
		else:
			echo "<br>ini masih tahun ".date("Y");
			echo "<br>Belum jelas 2012 kiamat apa tidak";
		endif; 
		echo "<br><br>";

		// syntax ringkas untuk alternatif if .. else
		$i = 5; 
		echo $i." adalah bilangan ";
		echo ($i % 2 == 0) ? "genap" : "ganjil";
		echo "<br>identik dengan:<br>";
		if ($i % 2 == 0)
			echo $i." adalah bilangan genap";
		else 
			echo $i." adalah bilangan ganjil";

	?>
</body>
</html>