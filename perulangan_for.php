<html>
<head>
	<title>PHP Control Structures : Loop dengan for/foreach</title>
</head>
<body>
	<h1>RPL PHP Programming 2019</h1>
	<h2>Perulangan menggunakan for dan foreach</h2>
	<?php 
		// membuat array berisi nama2 hari, dengan index 0-6
		$namahari = ["Minggu","Senin","Selasa","Rabu","Kamis","Jum'at","Sabtu"];
		echo "Nama-nama hari di Indonesia adalah : ";
		// count() untuk mendapatkan ukuran array
		for ($i=0; $i<count($namahari); $i++)
			echo $namahari[date($i)].", "; 
		echo "<br><br>Khusus array, kita bisa menggunakan foreach :";
		echo "<br>Nama-nama hari di Indonesia adalah : ";
		foreach ($namahari as $hari) 
			echo $hari.", ";
	?>
</body>
</html>